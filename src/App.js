import './App.css';
import React, {Component} from 'react';
import Input from './Components/Input';
import Messages from './Components/Messages';

function randomName() {
  const usernames = ["user1", "user2", "user3", "user4", "user5", "user6", "user7", "user8" ];
 
  const username = usernames[Math.floor(Math.random() * usernames.length)];
  return username;
}

function randomColor() {
  return '#' + Math.floor(Math.random() * 0xFFFFFF).toString(16);
}

class App extends Component {
  state = {
    messages: [],
    member: {
      username: randomName(),
      color: randomColor(),
    }
  }

  constructor() {
    super();
    this.drone = new window.Scaledrone("5NijQlfZGxlwjg1o", {
      data: this.state.member
    });
    this.drone.on('open', error => {
      if (error) {
        return console.error(error);
      }
      const member = {...this.state.member};
      member.id = this.drone.clientId;
      this.setState({member});
    });
    const room = this.drone.subscribe("observable-room");
    room.on('data', (data, member) => {
      const messages = this.state.messages;
      messages.push({member, text: data});
      this.setState({messages});
    });
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h1>Chat app Algebra</h1>
        </div>
        <Messages
          messages={this.state.messages}
          currentMember={this.state.member}
        />
        <Input
          onSendMessage={this.onSendMessage}
        />
      </div>
    );
  }

  onSendMessage = (message) => {
    console.log("message: ", message);
    this.drone.publish({
      room: "observable-room",
      message
    });
  }

}

export default App;